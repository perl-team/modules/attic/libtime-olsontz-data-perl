Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Time-OlsonTZ-Data
Source: https://metacpan.org/release/Time-OlsonTZ-Data
Upstream-Contact: Andrew Main (Zefram) <zefram@fysh.org>

Files: *
Copyright: 2010-2014, Andrew Main (Zefram) <zefram@fysh.org>
License: Artistic or GPL-1+

Files: tzsrc/*
Copyright: Public domain
License: public-domain
 The Olson timezone database was compiled by Arthur David Olson, Paul
 Eggert, and many others.  It is maintained by the denizens of the mailing
 list <tz@iana.org> (formerly <tz@elsie.nci.nih.gov>).

Files: debian/*
Copyright: 2012-2014, gregor herrmann <gregoa@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
